export default {
  myPlane: {
    // 初始坐标
    x: 246,
    y: 700,
    // 飞机宽高
    width: 258,
    height: 364,
    // 飞机移动速度
    speed: 10,
  },
  bullet: {
    hurt: 500, // 子弹 伤害
    speed: 10, // 子弹 速度
    width: 61,
    height: 99,
  },
  enemy: {
    x: () => Math.random().toFixed(2) * 400 + 100,
    y: 50,
    speed: () => Math.floor(Math.random() * 4) + 1, // 随机速度
    width: 308,
    height: 207,
    blood: 1000,
  },
};
