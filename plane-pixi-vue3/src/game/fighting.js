import { game } from '../game';
import { hitObjCheck } from '../utils';
import { onMounted, onUnmounted } from 'vue';

export const useFighting = (param) => {
  const { planeInfo, enemyList, bulletList, destroyBullet, destroyEnemy, gameOver } = param;

  const handleTicker = () => {
    bulletList.forEach((bullet, bIndex) => {
      enemyList.forEach((enemy, eIndex) => {
        if (hitObjCheck(bullet, enemy)) {
          destroyBullet(bIndex);

          enemy.blood -= bullet.hurt;

          if (enemy.blood <= 0) {
            destroyEnemy(eIndex);
          }
        }
      });
    });

    enemyList.forEach((enemy) => {
      if (hitObjCheck(enemy, planeInfo)) {
        gameOver();
      }
    });
  };

  onMounted(() => {
    game.ticker.add(handleTicker);
  });

  onUnmounted(() => {
    game.ticker.remove(handleTicker);
  });
};
