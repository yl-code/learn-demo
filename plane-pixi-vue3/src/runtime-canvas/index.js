import { createRenderer } from 'vue';
import { Container, Sprite, Texture, Text, Graphics } from 'pixi.js';

const renderer = createRenderer({
  createElement(type) {
    let element;
    switch (type) {
      case 'box': // box === container
        element = new Container();
        break;
      case 'sprite':
        element = new Sprite();
        break;
      case 'rect':
        element = new Graphics();
        element.beginFill(0xff000);
        element.drawRect(0, 0, 100, 100);
        element.endFill();
        break;
    }
    return element;
  },

  insert(el, parent) {
    if (el) {
      parent.addChild(el);
    }
  },

  remove(el) {
    if (el && el.parent) {
      el.parent.removeChild(el);
    }
  },

  parentNode(node) {
    if (node) {
      return node.parent;
    }
  },

  // 必须要的
  createText(text) {
    return new Text(text);
  },
  nextSibling() {},
  createComment() {},

  patchProp(el, key, prevVal, nextVal) {
    // console.log(key);
    switch (key) {
      case 'src':
        el.texture = Texture.from(nextVal);
        break;

      case 'onClick':
        el.on('pointertap', nextVal);
        break;
      default:
        // el.interactive = true
        // el.x = nextValue
        // el.y = nextValue
        el[key] = nextVal;
        break;
    }
  },
});

export const myCreateApp = (rootComponent) => renderer.createApp(rootComponent);
