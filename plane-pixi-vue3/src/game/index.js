import { Application } from 'pixi.js';

export const game = new Application({
  width: 750,
  height: 1080,
  backgroundColor: 0x6dcccc,
});

document.body.append(game.view);

export const getRootContainer = () => game.stage;
