import App from './App.vue';
import { getRootContainer } from './game';
import { myCreateApp } from './runtime-canvas';

console.warn = () => {};

myCreateApp(App).mount(getRootContainer());
